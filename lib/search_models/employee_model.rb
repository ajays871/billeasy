class SearchModels::EmployeeModel
	def search_fields
		{
			name: { field: 'employees.first_name', join: nil },
			department: { field: 'departments.name', join: "departments" },
			title: { field: 'titles.name', join: "titles" }
		}
	end

	def sort_fields
		{
			name: { field: 'employees.name', join: nil }
		}
	end
end

class SearchModels::DepartmentModel
	def search_fields
		{
			name: { field: 'departments.name', join: nil }			
		}
	end

	def sort_fields
		{
			name: { field: 'name', join: nil }
		}
	end
end

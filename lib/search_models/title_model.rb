class SearchModels::TitleModel
	def search_fields
		{
			name: { field: 'titles.name', join: nil }			
		}
	end

	def sort_fields
		{
			name: { field: 'name', join: nil }
		}
	end
end

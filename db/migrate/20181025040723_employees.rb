class Employees < ActiveRecord::Migration[5.1]
  def change
  	create_table :employees do |t|
	  	t.string :first_name,              null: false, default: ""
	    t.string :last_name, 				null: false,  default: ""
	    t.references :department
	    t.references :title
	end
  end
end

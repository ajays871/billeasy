class Titles < ActiveRecord::Migration[5.1]
  def change
  	create_table :titles do |t|
  		t.string :name,              null: false, default: ""
  	end
  end
end

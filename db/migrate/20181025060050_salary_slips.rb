class SalarySlips < ActiveRecord::Migration[5.1]
  def change
  	create_table :salary_slips do |t|
  		t.string :month,              null: false, default: ""
  		t.integer :salary, null: false, default: 0
  		t.references :employees
  		t.integer :employee_id, null: false, default: 0, index: true
  	end
  end
end

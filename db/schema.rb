# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181025060050) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "departments", force: :cascade do |t|
    t.string "name", default: "", null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string "first_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.bigint "department_id"
    t.bigint "title_id"
    t.index ["department_id"], name: "index_employees_on_department_id"
    t.index ["title_id"], name: "index_employees_on_title_id"
  end

  create_table "salary_slips", force: :cascade do |t|
    t.string "month", default: "", null: false
    t.integer "salary", default: 0, null: false
    t.bigint "employees_id"
    t.integer "employee_id", default: 0, null: false
    t.index ["employee_id"], name: "index_salary_slips_on_employee_id"
    t.index ["employees_id"], name: "index_salary_slips_on_employees_id"
  end

  create_table "titles", force: :cascade do |t|
    t.string "name", default: "", null: false
  end

end

class ResourceController < ApplicationController
   		
   	def capitalized_resource_name
   		resource_name.to_s.split('_').map{|e| e.capitalize}.join
   	end

   	def create	   		   		   	    	   	   
   		resource = capitalized_resource_name.constantize.new(resource_params)
   		resource.save!
   		
   		render json: {	   		
   			resource_name => ActiveModelSerializers::SerializableResource.new(resource, {})
   		}
   	end

   	def index	   		
   		page = params[:page] || 1
   		size = params[:size] || 10
   		resources = nil   					
   		resources = Search::SearchManager.new(resource_name, params[:q], page, size, index_preloads).search   				   		
        render json: {status: 'SUCCESS', page: page, size: size, attributes:  capitalized_resource_name.constantize.column_names ,data: ActiveModelSerializers::SerializableResource.new(resources, {})}, status: :ok
	end

	def update
		resource = capitalized_resource_name.constantize.find(params[:id])
		resource.update(resource_params)
		render json: {
			result: resource
		}
	end

	def show
		resource = capitalized_resource_name.constantize.eager_load(show_preloads).find(params[:id])					
		render json: resource
	end 

   	private
   	def resource_params
		params.require(resource_name).permit(permitted_attributes)
	end
	  
	private
	# Use the following function to ensure only logged in members can access the API
end


class SalarySlipsController < ResourceController

	def resource_name
		:salary_slip
	end

	private

	def permitted_attributes					
		[:month, :salary, :employee_id]
	end

	def index_preloads
		[:employee]
	end

	def show_preloads		
		[:employee]
	end
end
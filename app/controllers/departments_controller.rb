
class DepartmentsController < ResourceController

	def resource_name
		:department
	end

	private

	def permitted_attributes						
		[:name]
	end

	def index_preloads
		# [:account]
	end

	def show_preloads
		# [:account]
	end
end


class EmployeesController < ResourceController

	def resource_name
		:employee
	end

	private

	def permitted_attributes					
		[:first_name, :last_name, :department_id, :title_id]
	end

	def index_preloads
		[:department, :title]
	end

	def show_preloads		
		[:department, :title, :salary_slip]
	end
end
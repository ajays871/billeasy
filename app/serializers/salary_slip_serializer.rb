class SalarySlipSerializer < ActiveModel::Serializer
  attributes :id, :month, :salary
end

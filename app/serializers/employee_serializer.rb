class EmployeeSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :title_id, :department_id
  attribute :department
  attribute :title
  has_many :salary_slip
end

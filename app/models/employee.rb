class Employee < ApplicationRecord
	belongs_to :department
	belongs_to :title
	has_many :salary_slip
end